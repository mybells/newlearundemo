﻿/* * 版 本 Learun-ADMS V7.0.0 力软敏捷开发框架(http://www.learun.cn)
 * Copyright (c) 2013-2018 上海力软信息技术有限公司
 * 创建人：超级管理员
 * 日  期：2018-12-27 14:44
 * 描  述：DEMO
 */
var selectedRow;
var refreshGirdData;
var bootstrap = function ($, learun) {
    "use strict";
    var page = {
        init: function () {
            page.initGird();
            page.bind();
        },
        bind: function () {
            // 查询
            $('#btn_Search').on('click', function () {
                var keyword = $('#txt_Keyword').val();
                page.search({ keyword: keyword });
            });
            // 刷新
            $('#lr_refresh').on('click', function () {
                location.reload();
            });
            // 新增
            $('#lr_add').on('click', function () {
                selectedRow = null;
                learun.layerForm({
                    id: 'form',
                    title: '新增',
                    url: top.$.rootUrl + '/LR_CodeDemo/DEMO/Form',
                    width: 700,
                    height: 400,
                    callBack: function (id) {
                        return top[id].acceptClick(refreshGirdData);
                    }
                });
            });
            // 编辑
            $('#lr_edit').on('click', function () {
                var keyValue = $('#gridtable').jfGridValue('F_UserId');
                selectedRow = $('#gridtable').jfGridGet('rowdata');
                if (learun.checkrow(keyValue)) {
                    learun.layerForm({
                        id: 'form',
                        title: '编辑',
                        url: top.$.rootUrl + '/LR_CodeDemo/DEMO/Form?keyValue=' + keyValue,
                        width: 700,
                        height: 400,
                        callBack: function (id) {
                            return top[id].acceptClick(refreshGirdData);
                        }
                    });
                }
            });
            // 删除
            $('#lr_delete').on('click', function () {
                var keyValue = $('#gridtable').jfGridValue('F_UserId');
                if (learun.checkrow(keyValue)) {
                    learun.layerConfirm('是否确认删除该项！', function (res) {
                        if (res) {
                            learun.deleteForm(top.$.rootUrl + '/LR_CodeDemo/DEMO/DeleteForm', { keyValue: keyValue}, function () {
                            });
                        }
                    });
                }
            });
        },
        initGird: function () {
            $('#gridtable').lrAuthorizeJfGrid({
                url: top.$.rootUrl + '/LR_CodeDemo/DEMO/GetPageList',
                headData: [
                        { label: '用户主键', name: 'F_UserId', width: 200, align: "left" },
                        { label: '工号', name: 'F_EnCode', width: 200, align: "left" },
                        { label: '登录账户', name: 'F_Account', width: 200, align: "left" },
                        { label: '登录密码', name: 'F_Password', width: 200, align: "left" },
                        { label: '密码秘钥', name: 'F_Secretkey', width: 200, align: "left" },
                        { label: '真实姓名', name: 'F_RealName', width: 200, align: "left" },
                        { label: '呢称', name: 'F_NickName', width: 200, align: "left" },
                        { label: '头像', name: 'F_HeadIcon', width: 200, align: "left" },
                        { label: '快速查询', name: 'F_QuickQuery', width: 200, align: "left" },
                        { label: '简拼', name: 'F_SimpleSpelling', width: 200, align: "left" },
                        { label: '性别', name: 'F_Gender', width: 200, align: "left" },
                        { label: '生日', name: 'F_Birthday', width: 200, align: "left" },
                        { label: '手机', name: 'F_Mobile', width: 200, align: "left" },
                        { label: '电话', name: 'F_Telephone', width: 200, align: "left" },
                        { label: '电子邮件', name: 'F_Email', width: 200, align: "left" },
                        { label: 'QQ号', name: 'F_OICQ', width: 200, align: "left" },
                        { label: '微信号', name: 'F_WeChat', width: 200, align: "left" },
                        { label: 'MSN', name: 'F_MSN', width: 200, align: "left" },
                        { label: '机构主键', name: 'F_CompanyId', width: 200, align: "left" },
                        { label: '部门主键', name: 'F_DepartmentId', width: 200, align: "left" },
                        { label: '安全级别', name: 'F_SecurityLevel', width: 200, align: "left" },
                        { label: '单点登录标识', name: 'F_OpenId', width: 200, align: "left" },
                        { label: '密码提示问题', name: 'F_Question', width: 200, align: "left" },
                        { label: '密码提示答案', name: 'F_AnswerQuestion', width: 200, align: "left" },
                        { label: '允许多用户同时登录', name: 'F_CheckOnLine', width: 200, align: "left" },
                        { label: '允许登录时间开始', name: 'F_AllowStartTime', width: 200, align: "left" },
                        { label: '允许登录时间结束', name: 'F_AllowEndTime', width: 200, align: "left" },
                        { label: '暂停用户开始日期', name: 'F_LockStartDate', width: 200, align: "left" },
                        { label: '暂停用户结束日期', name: 'F_LockEndDate', width: 200, align: "left" },
                        { label: '排序码', name: 'F_SortCode', width: 200, align: "left" },
                        { label: '删除标记', name: 'F_DeleteMark', width: 200, align: "left" },
                        { label: '有效标志', name: 'F_EnabledMark', width: 200, align: "left" },
                        { label: '备注', name: 'F_Description', width: 200, align: "left" },
                        { label: '创建日期', name: 'F_CreateDate', width: 200, align: "left" },
                        { label: '创建用户主键', name: 'F_CreateUserId', width: 200, align: "left" },
                        { label: '创建用户', name: 'F_CreateUserName', width: 200, align: "left" },
                        { label: '修改日期', name: 'F_ModifyDate', width: 200, align: "left" },
                        { label: '修改用户主键', name: 'F_ModifyUserId', width: 200, align: "left" },
                        { label: '修改用户', name: 'F_ModifyUserName', width: 200, align: "left" },
                ],
                mainId:'F_UserId',
                isPage: true
            });
            page.search();
        },
        search: function (param) {
            param = param || {};
            $('#gridtable').jfGridSet('reload', { queryJson: JSON.stringify(param) });
        }
    };
    refreshGirdData = function () {
        page.search();
    };
    page.init();
}
